# Other Interesting Papers in our Domains

- [2023-Oct - "VOCOS: Closing the Gap between Time-Domain and Fourier-Based Neural Vocoders for High-Quality Audio Synthesis"](Vocos-2306.00814.pdf)
  <br/>
  [[Medium article](https://ngwaifoong92.medium.com/introduction-to-vocos-fast-neural-vocoder-e055a27bbf92)]
  <br/>
  [[Code](https://github.com/charactr-platform/vocos)]

- [2023-Jan - "SingSong: Generating musical accompaniments from singing"](SingSong-2301.12662.pdf)
  [[Examples](https://g.co/magenta/singsong)]

- [2022-Mar - ConvNeXt: "A ConvNet for the 2020s"](ConvNeXt-2201.03545.pdf)
  <br/>
  [[Medium article](https://medium.com/augmented-startups/convnext-the-return-of-convolution-networks-e70cbe8dabcc)]
  <br/>
  [[Code](https://github.com/facebookresearch/ConvNeXt)]
- [2022-Mar - "ISTFTNet: Fast and Lightweight Mel-Spectrogram Vocoder Incorporating Inverse Short-Time Fourier Transform"](ISTFTNet-2203.02395.pdf)
  <br/>
  [[Audio Samples](https://www.kecl.ntt.co.jp/ people/kaneko.takuhiro/projects/istftnet/)]
- [2021-Aug - "GAN Vocoder: Multi-Resolution Discriminator Is All You Need"](GANVocoder-2103.05236.pdf)
  <br/>
  [[Code](https://moneybrain-research.github.io/gan-vocoder)]
- [2021-Jun - "UnivNet: A Neural Vocoder with Multi-Resolution Spectrogram Discriminators for High-Fidelity Waveform Generation"](UnivNet-2106.07889.pdf)
  [[Demo](https://kallavinka8045.github.io/is2021/)]
- [2020-Oct - "HiFi-GAN: Generative Adversarial Networks for Efficient and High Fidelity Speech Synthesis"](HiFiGAN-2020-2010.05646.pdf)]
  <!-- Multi-period discriminator -->
  <br/>
  [[Demo](https://jik876.github.io/hifi-gan-demo/)]
  <br/>
  [[Code](https://github.com/jik876/hifi-gan)]
  <br/>
- [2019-Oct - "MelGAN: Generative Adversarial Networks for Conditional Waveform Synthesis"](MelGAN-2019-1910.06711.pdf)
- [2019-Apr - "GANSynth: Adversarial Neural Audio Synthesis"](GANSynth-1902.08710.pdf)
  [[Colab Notebook](http://goo.gl/magenta/gansynth-demo)]
  [[Audio Examples](http://goo.gl/magenta/gansynth-examples)]
  [[Code](http://goo.gl/magenta/gansynth-code)]

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
