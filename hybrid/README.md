# Recent Papers Combining Attention, Gated RNNs, Etc.

- [2023-Jan-28 - "Mega: Moving Average Equipped Gated Attention"](Mega-2209.10655.pdf)  [[Code]](https://github.com/facebookresearch/mega)
- [2024-Feb-28 - Based: "Simple linear attention language models balance the recall-throughput tradeoff"](Based-2402.18668.pdf)  [[Code]](https://github.com/HazyResearch/based)
- [2024-Mar-28 - "Jamba: A Hybrid Transformer-Mamba Language Model"](Jamba-2403.19887.pdf) [[Model Card]](https://huggingface.co/ai21labs/Jamba-v0.1)
- [2024-Apr-16 - "Megalodon: Efficient LLM Pretraining and Inference with Unlimited Context Length"](Megalodon-2404.08801.pdf) [[Code]](https://github.com/XuezheMax/megalodon)
- [2024-Apr-26 - Mambaformer: "Integrating Mamba and Transformer for Long-Short Range Time Series Forecasting"](Mambaformer-2404.14757v1.pdf) [[Code]](https://github.com/XiongxiaoXu/Mambaformer-in-Time-Series)
- [2024-May-26 - "Zamba: A Compact 7B SSM Hybrid Model"](Zamba-2405.16712v1.pdf)
- [2024-Jun-11 - "SAMBA: Simple Hybrid State Space Models for Efficient Unlimited Context Language Modeling"](Samba-2406.07522v1.pdf)

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
