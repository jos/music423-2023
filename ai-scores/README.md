# Sequence Modeling for Music at the symbolic level (scores, MIDI, ...)

- [Music Transformer](MusicTransformer-1809.04281.pdf)
  [[Supplementary](https://magenta.tensorflow.org/music-transformer)
- [Tonic Net](TonicNet-1911.11775.pdf)
- [Anticipatory Music Transformer](Anticipatory-2306.08620.pdf)
  [[Supplementary](https://johnthickstun.com/anticipation/)]
- [Natural Language Symbolic Music Survey](NatLangSymbolicMusicSurvey-2402.17467v1.pdf)
- [Hierarchical multi-head attention LSTM for polyphonic symbolic melody generation](HierMultiHeadAttentionLSTM-PolyMelody-s11042-024-18491-7--2024-02-08.pdf.pdf)

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
