# Courses and Online Tutorials
- [Zero to Hero series](https://karpathy.ai/zero-to-hero.html) by Andrej Karpathy<br/>
  This was sufficient for JOS due to his signal processing background, learning Python and PyTorch along the way.<br/>
  ChatGPT-4 helped a lot with learning Python and PyTorch:  Paste your cryptic Python code and get a clear breakdown.<br/>
  Music 320a is sufficient DSP, if fully absorbed, and 320b is helpful as well.
- [Deep Learning at CMU](https://deeplearning.cs.cmu.edu/S24/index.html)
- [Machine Learning Specialization](https://www.coursera.org/specializations/machine-learning-introduction) by Andrew Ng et al. (3 courses)<br/>
  This uses TensorFlow, but JOS is sticking with PyTorch until the more general graph construction features of TF are needed.
- [Deep Learning Specialization](https://www.deeplearning.ai/courses/machine-learning-specialization/) by Andrew Ng et al. (5 courses)<br/>
  CCRMA graduate students have said this series is "highly recommended" - also TensorFlow based.

# Key Papers Followed in the [Zero to Hero series](https://karpathy.ai/zero-to-hero.html)
- MLP: [Bengio et al. 2003](https://www.jmlr.org/papers/volume3/bengio03a/bengio03a.pdf)
- CNN: [DeepMind WaveNet 2016](https://arxiv.org/abs/1609.03499) (not yet implemented in [makemore](https://github.com/karpathy/makemore.git))
- RNN: [Mikolov et al. 2010](https://www.fit.vutbr.cz/research/groups/speech/publi/2010/mikolov_interspeech2010_IS100722.pdf)
- LSTM: [Graves et al. 2014](https://arxiv.org/abs/1308.0850) (also not implemented in [makemore](https://github.com/karpathy/makemore.git), preferring the GRU)
- GRU: [Kyunghyun Cho et al. 2014](https://arxiv.org/abs/1409.1259)
- Transformer: [Vaswani et al. 2017](https://arxiv.org/abs/1706.03762)
- [Mamba](../mamba/Mamba-2312.00752.pdf) should be easy to add, and JOS plans to do that if he gets to it first

# History of Modern AI Technology (In Progress)

- [The moment we stopped understanding AI [AlexNet] - Welch Labs](https://youtu.be/UZDiGooFs54)

- [Ganesh Sivaraman's Historical Summary](https://www.youtube.com/watch?v=wvkD36nzs50&list=PLuUi5wq5RvigY2q5V6ldaaV9oJ-81pZ8x&t=130s)
   - 1943: McCulloch-Pitts neuron model
   - 1957: Rosenblatt's Perceptron (weights, biases, binary-step activation)
   - 1960: Stochastic Gradient Descent (Widrow-Hoff LMS algorithm)
   - 1965: High-dimensional embeddings for linear separability of nonlinear classification (Tom Cover)
   - 60s-70s: Backpropagation learning rule discovered<br/> (nobody seems to have a solid date on this; JOS only has references in the 80s)
   - 1986: Rumelhart, Hinton, and Williams (backpropagation clearly understood, used, and described - earliest reference JOS has)
   - 1987: Rumelhart and McClelland (book, "connectionist" networks)
   - 90s: Convolutional Neural Networks (CNN)
   - 2009: Raina et al. (deep learning takes over due to use of GPUs for training)<br/> [JOS is surprised not to see AlexNet here]
   - 2010: Ciresan et al. (")
   - [2015: Modeling phoneme categories](http://labrosa.ee.columbia.edu/cuneuralnet/nagamine101415.pdf)
   - [Notes](SivaramanNotes.txt)
- Michael Bronstein's Overview: [Video: First half hour: (MLSS Kraków 2023))](https://www.youtube.com/watch?v=hROSXAY2JBc)
  <!-- - [Paper on Geometric Deep Learning (not so much history)](BronsteinGeometricDL-1611.08097.pdf) -->
- [Schmidhuber's Annotated History of Modern AI and Deep Learning](SchmidhuberHistory-2212.11279.pdf)
   - [Online version](https://people.idsia.ch/~juergen/deep-learning-history.html)

## Notes

- Relative to my prior exposure in this area, and Bronstein's video
  linked above, Schmidhuber's history is incomplete and oddly slanted
  (see [this tweet](https://www.reddit.com/r/MachineLearning/comments/zetvmd/comment/iz8kqp3/)).
  However, it also adds a lot to what I've seen, so I like having it
  on our list.  We will need unbiased historians to sort everything
  out properly.  Schmidhuber was second author on the original LSTM
  paper, and is presently director of the KAUST AI Initiative in Saudi
  Arabia, so he's definitely a great source of pointers.

    - The McCulloch-Pitts Neuron, usually given as the starting point
      for neural nets in what I've read, is summarized in one sentence
      as follows: "Non-learning RNNs were also discussed in 1943 by
      neuroscientists Warren McCulloch und Walter Pitts and formally
      analyzed in 1956 by Stephen Cole Kleene".

    - Rosenblatt's Perceptron is only mentioned in a footnote, and his
      work is strangely characterized a special case of MultiLayer
      Perceptrons without proper attribution. Compare that to
      Bronstein's coverage starting at t=9m.

    - No mention of the Widrow-Hoff LMS algorithm, which I understood to
      be a spin-off of early neural net research by Widrow et al. There is
      a 1962 Widrow and Hoff's citation, however, which may contain the
      essence of LMS.

    - No mention of the 1969 multistage Backpropagation by Bryson and Ho

    - I suspect Schmidhuber has been generally offended by
      sloppy/spotty scholarship in the field and is reacting to that
      in his account.  I agree that the academic scholarship is way
      too sparse and shallow, but hey, things move super-fast in this
      field, and most researchers aren't that interested in the early
      history.  People tend to propagate the historical summaries of
      related recent papers.  Later on, more scholarly researchers
      come along and rectify the record.  This tends to happen slowly
      but surely, in my experience.  It took quite a while for example
      to properly credit Gauss for the Fast Fourier Transform (FFT).

    - In any case, this boatload of historical references is very much
      appreciated!  We just need more references to balance it all
      out, and unbiased historians to work out the full history
      dispassionately from the published record.

- Here is my own highly incomplete and unpublished timeline of main developments cobbled together over recent years based on various reading.
  I plan to flesh it out further from the above links at some point:

    - 1847 - Gradient Descent (Cauchy)
    - 1943 - McCulloch-Pitts Neuron:  $ y = \underline{w}^T \underline{x}> \theta $
    - 1957 - Perceptron (Rosenblatt):  $ y = \sigma(\underline{w}^T [1; \underline{x}]) $
    - 1960 - Multistage Optimal Control by Gradient Descent (Kelly, Bryson)
             LMS Algorithm (Widrow, Hoff)
    - 1960s - Multilayer Perceptrons (MLP)
              Universality of a single hidden-layer figured out
    - 1969 - Multistage Backpropagation (Bryson and Ho)
    - 1980s - Backpropagation through a Differentiable Nonlinear Activation Function (such as a tanh)
    - 1986 - Rumelhart, Hinton, Williams et al. describe backprop in its current form (LeCun close in '85)
    - 2000s - Internet (the needed data)
    - 2010s - GPUs and Moore's law (the computation)
    - 2012 - Deep Learning beats HMMs/GMMs at Speech Recognition (Hinton et al.)
    - 2012 - AlexNet (CNN) wins on ImageNet Large Scale Visual Recognition Challenge using the first large-scale deep neural network [Krizhevsky et al.]
    - 2013 - Variational AutoEncoder (VAE) proposed by Knigma and Welling at Google and Qualcomm
    - 2014 - Generative Adversarial Network (GAN) proposed by Ian Goodfellow et al.
    - 2015 - Batch normalization (``batch norm'') proposed by Sergey Ioffe and Christian Szegedy
    - 2016 - Google Translate switches from ``phrase-based statistical machine translation'' to deep neural nets (LSTM)
    - 2016 - AlphaGo beats Lee Sedol
    - 2017 - Emergent ``sentiment neuron'' from unsupervised prediction of next character in Amazon reviews
    - 2017 - Attention Transformer drops [Vaswani et al.]
    - 2018 - GPT-1 (117M weights)
    - 2018 - AlphaFold 1 wins protein folding competition (almost 60/100)
    - 2019 - GPT-2 (1.5B weights) - freely available - see HuggingFace.co
    - 2020 - GPT-3 (175B weights)
    - 2020 - ``Scaling Laws for Neural Language Models'' by Kaplan et al.
    - 2021 - AlphaFold 2 wins protein folding competition again by more (over 90/100 for 2/3  exp't)

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
