# Music 423 2023 GitLab Project: Research Paper Collection

<!-- Right now we're mainly only sharing PDF files with this repo (see next section). -->

## Shared PDF Directories
- [Music Audio Generation](./ai-music-audio-gen/README.md)
- [WaveNet](./wavenet/README.md)
- [Audio Codecs](./ai-audio-codecs/README.md)
- [Music Sequence Generation (Scores, MIDI)](./ai-scores/README.md)
- [Differentiable DSP (DDSP)](./ddsp/README.md)
- [Synth/Effect Parameter Estimation](./synth-effect-matching/README.md)
- [Reverse Diffusion](./diffusion/README.md)
- [Mamba](./mamba/README.md)
- [Attention](./attention/README.md)
- [Hybrid](./hybrid/README.md)
- [Miscellaneous (Spectrogram Inversion, Accompaniment Generation, ...)](./ai-misc/README.md)
- [Courses, Videos, and History](./history/README.md)

Ideally you can read the latest paper(s) in each category, and only
refer to the earlier papers as needed.  However, I prefer reading them
all in order, skimming where redundant or obsolete.  This spends more
time but solidifies understanding. Let JOS know if you need earlier
papers or background references on anything.

## Past Presentations
- [Aut 2023](./admin/Schedule-2023-aut.md)

## Usage
For now, follow <code>README.md</code>

We can also view the repo by <code> [music423-2023/index.html](index.html) </code> in local browser, but since we are not publishing the website, markdown files provide better readablity on Gitlab.

<!-- 

---

## Netlify Configuration

Thanks to the GitLab template chosen, the <a href="./public/"><tt>public</tt> </a> subdirectory can be published using [Netlify](https://www.netlify.com/)

In order to build this site with Netlify, simply log in or register at 
https://app.netlify.com/, then select "New site from Git" from the top
right. Select GitLab, authenticate if needed, and then select this
project from the list. 

You will need to set the publish directory to `/public`. Netlify will handle the 
rest.

In the meantime, you can take advantage of all the great GitLab features
like merge requests, issue tracking, epics, and everything else GitLab has
to offer.

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html -->
