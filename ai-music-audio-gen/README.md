# Papers on AI Music Audio Generation in Chronological Order

### 

- [2016 - WaveNet: “A Generative Model for Raw Audio”](WaveNet-2016-1609.03499.pdf)
- [2017 - NSynth: “Neural Audio Synthesis of Musical Notes with WaveNet Autoencoders”](NSynth-2017-1704.01279.pdf)
- [2017 - SampleRNN: “Generating Albums with SampleRNN . . . ” [Dadabots]](SampleRNN-1612.07837.pdf)
- [2019 - MusicVAE: “A Hier. Latent Vector Model for Learning L.T. Structure in Music”](MusicVAE-1803.05428.pdf)
- [2020 - Jukebox: “A Generative Model for Music”](Jukebox-2005.00341.pdf)
- [2022 - [Review]: “A Review of Intelligent Music Generation Systems”](MusicGenReview_2022-11-16_2211.09124.pdf)
- [2022 - “AudioLM: a Language Modeling Approach to Audio Generation”](AudioLM-2209.03143.pdf)
- [2023 - “MusicLM: Generating Music From Text” [Jan]](MusicLM-2301.11325.pdf)
- [2023 - MusicGen: “Simple and Controllable Music Generation” [Jun]](MusicGen-2306.05284.pdf)
- [2023 - "VampNet: Music Generation via Masked Acoustic Token Modeling" [Jul]](VampNet-2307.04686.pdf)
- [2024 - "Masked Audio Generation Using a Single Nonautoregressive Transformer" [Jan]](MAGNeT-2401.04577.pdf) [[WebPage]](https://pages.cs.huji.ac.il/adiyoss-lab/MAGNeT/)
- [2024 - "Long-Form Music Generation with Latent Diffusion"](DiffusionLongFormMusic-2404.10301.pdf)

## Notes

###

- Latest systems like MusicGen and MusicLM use [vector-quantizing audio codecs](../ai-audio-codecs/README.md) to convert audio into a sequence of vectors suitable for sequence modeling by transformers and such.
  - [2023 - "Stack-and-Delay: A New Codebook Pattern for Music Generation" [Sep]](StackAndDelay-2309.08804.pdf)<br/>
    Extends the codebook patterns of [MusicGen](MusicGen-2306.05284.pdf) for faster high-quality inference

- Check out [Dadabots “lofi classic metal ai radio”](https://youtu.be/J1NV6CUJl18) based on [SampleRNN](SampleRNN-1612.07837.pdf)

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
			``
