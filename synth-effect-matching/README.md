# Papers on neural methods for matching synthesizer/effect parameters to desired sounds

- [2018-04 - Automatic Programming of VST Sound Synthesizers Using Deep Networks and Other Techniques](https://ieeexplore-ieee-org.stanford.idm.oclc.org/stamp/stamp.jsp?tp=&arnumber=8323327)
- [2018-12 - InverSynth: Deep Estimation of Synthesizer Parameter Configurations from Audio Signals](InverSynth-1812.06349.pdf)
- [2019-12 - Flow Synthesizer: Universal Audio Synthesizer Control with Normalizing Flows](FlowSynth-applsci-10-00302-v2.pdf)
- [2021-11 - Synthesizer Sound Matching with Differentiable DSP (ISMIR-21)]( ../ddsp/SynthMatchingDDSP-ISMIR21-53.pdf)
- [2021-05 - [DeepAFx] Differentiable Signal Processing with Black-Box Audio Effects](DeepAFx21.pdf)
- [2022-07 - [DeepAFx-ST] Style Transfer of Audio Effects with Differentiable Signal Processing](../ddsp/DeepAFX-ST-2207.08759v1.pdf) [(code)](https://github.com/adobe-research/DeepAFx-ST)
- [2022-07 - Sound2Synth: Interpreting Sound via FM Synthesizer Parameters Estimation](Sound2Synth-2205.03043v2.pdf)
- [2023-01 - Improving Semi-Supervised Differentiable Synthesizer Sound Matching for Practical Applications](../ddsp/SynthMatchingDDSP-2023.pdf)
- [2023-03 - Quality-diversity for Synthesizer Sound Matching](MasudaAndSaito2023-QualityDiversity-SoundMatching-31_220.pdf)
- [2024-01 - DIFFMOOG: A Differentiable Modular Synthesizer for Sound Matching](../ddsp/DiffMoog-2401.12570.pdf)
- [2024-07 - Synthesizer Sound Matching Using Audio Spectrogram Transformers (DAFx-24)](MassiveMatchingNI-DAFx-2407.16643v1.pdf)
             (based on [AST](../attention/AST-AudioSpectrogramTransformer-2104.01778v3.pdf))

---

[Notes](Notes.md)

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
