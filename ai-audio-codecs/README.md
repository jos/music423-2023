# Main Recent Papers on AI Audio Codecs

- [2021 - "SoundStream: An End-to-End Neural Audio Codec"](SoundStream-2021-2107.03312.pdf) [[HomePage](https://blog.research.google/2021/08/soundstream-end-to-end-neural-audio.html?m=1)] [[Code](https://github.com/wesbz/SoundStream)]
- [2022 - EnCodec: "High Fidelity Neural Audio Compression"](EnCodec-2210.13438.pdf) [[HomePage](https://huggingface.co/docs/transformers/main/model_doc/encodec)] [[Code](https://github.com/facebookresearch/encodec)]
- [2023 - Descript: "High-Fidelity Audio Compression with Improved RVQGAN" [Jun]](Descript-RVQGAN-2306.06546.pdf) [[HomePage](https://huggingface.co/descript/descript-audio-codec)] [[Code](https://github.com/descriptinc/descript-audio-codec)]
- [2023 - FSQ: "Finite Scalar Quantization: VQ-VAE Made Simple" [Sep]](FSQ-FiniteScalarQuantization-2309.15505.pdf) [See paper for code]

## With More Precursors and Side Branches

- [1984 - Vector Quantization (VQ)](VQ-VectorQuantization-Gray84.pdf)
- [2013 - Variational AutoEncoder (VAE)](VAE-Intro-1906.02691.pdf)
- [2017 - NSynth: "Neural Audio Synthesis of Musical Notes with WaveNet Autoencoders"](../ai-music-audio-gen/NSynth-2017-1704.01279.pdf)
- [2018 - VQ-VAE: "Neural Discrete Representation Learning"](VQ-VAE-NeuralDiscreteRepresentationLearning-2018-1711.00937.pdf)
- [2019 - "GANSynth: Adversarial neural audio synthesis" [Apr]](GANSynth-1902.08710.pdf) [[HomePage](https://magenta.tensorflow.org/gansynth)] [[Code](http://goo.gl/magenta/gansynth-code)]
- [2019 - "MelGAN: Generative Adversarial Networks for Conditional Waveform Synthesis" [Dec]](MelGAN-2019-1910.06711.pdf)
- [2020 - AudioEnhancerGAN: "Audio Codec Enhancement with Generative Adversarial Networks"](AudioEnhancerGAN-2020-2001.09653.pdf)
- [2021 - "AST: Audio Spectrogram Transformer"](AST-AudioSpectrogramTransformer-2104.01778v3.pdf)
- [2021 - "SoundStream: An End-to-End Neural Audio Codec"](SoundStream-2021-2107.03312.pdf)
- [2022 - EnCodec: "High Fidelity Neural Audio Compression"](EnCodec-2210.13438.pdf)
- [2023 - "SoundStorm: Efficient Parallel Audio Generation" [May]](SoundStorm-2305.09636.pdf)
- [2023 - Descript: "High-Fidelity Audio Compression with Improved RVQGAN" [Jun]](Descript-RVQGAN-2306.06546.pdf)
- [2023 - FSQ: "Finite Scalar Quantization: VQ-VAE Made Simple" [Sep]](FSQ-FiniteScalarQuantization-2309.15505.pdf)

## Notes

- FSQ is intended to replace VQ-VAE, so read that before grappling with optimizing your own VQ-VAE codebook
- Descript (used in [VampNet](../ai-music-audio-gen/VampNet-2307.04686.pdf)) is SOTA and provides code
  - Nicely readable printout of the Descript [`modules`](DescriptNotes.md) data structure in the Python debugger `pdb`.
    + The functions can be easily Web-searched to find their Pytorch documentation and implementations.

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
