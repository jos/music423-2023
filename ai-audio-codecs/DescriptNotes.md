I was pleasantly surprised by Descript's clean `modules` data structure when printed in the Python debugger `pdb`.<br/>
I've added hyperlinks to supporting documentation/code for the first instance of some of the functions.
Feel free to add more!

<big><pre>
(Pdb) p modules
[OrderedDict](https://docs.python.org/3/library/collections.html#ordereddict-objects)([('encoder', [Encoder](https://github.com/descriptinc/descript-audio-codec/blob/main/dac/model/dac.py#L64)(
  (block): [Sequential](https://pytorch.org/docs/stable/generated/torch.nn.Sequential.html)(
    (0): [Conv1d](https://pytorch.org/docs/stable/generated/torch.nn.Conv1d.html)(1, 64, kernel_size=(7,), stride=(1,), padding=(3,))
    (1): [EncoderBlock](https://github.com/descriptinc/descript-audio-codec/blob/main/dac/model/dac.py#L43)(
      (block): Sequential(
        (0): [ResidualUnit](https://github.com/descriptinc/descript-audio-codec/blob/main/dac/model/dac.py#L24)(
          (block): Sequential(
            (0): [Snake1d](https://github.com/descriptinc/descript-audio-codec/blob/main/dac/nn/layers.py#L27)()
            (1): Conv1d(64, 64, kernel_size=(7,), stride=(1,), padding=(3,))
            (2): Snake1d()
            (3): Conv1d(64, 64, kernel_size=(1,), stride=(1,))
          )
        )
        (1): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(64, 64, kernel_size=(7,), stride=(1,), padding=(9,), dilation=(3,))
            (2): Snake1d()
            (3): Conv1d(64, 64, kernel_size=(1,), stride=(1,))
          )
        )
        (2): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(64, 64, kernel_size=(7,), stride=(1,), padding=(27,), dilation=(9,))
            (2): Snake1d()
            (3): Conv1d(64, 64, kernel_size=(1,), stride=(1,))
          )
        )
        (3): Snake1d()
        (4): Conv1d(64, 128, kernel_size=(4,), stride=(2,), padding=(1,))
      )
    )
    (2): EncoderBlock(
      (block): Sequential(
        (0): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(128, 128, kernel_size=(7,), stride=(1,), padding=(3,))
            (2): Snake1d()
            (3): Conv1d(128, 128, kernel_size=(1,), stride=(1,))
          )
        )
        (1): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(128, 128, kernel_size=(7,), stride=(1,), padding=(9,), dilation=(3,))
            (2): Snake1d()
            (3): Conv1d(128, 128, kernel_size=(1,), stride=(1,))
          )
        )
        (2): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(128, 128, kernel_size=(7,), stride=(1,), padding=(27,), dilation=(9,))
            (2): Snake1d()
            (3): Conv1d(128, 128, kernel_size=(1,), stride=(1,))
          )
        )
        (3): Snake1d()
        (4): Conv1d(128, 256, kernel_size=(8,), stride=(4,), padding=(2,))
      )
    )
    (3): EncoderBlock(
      (block): Sequential(
        (0): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(256, 256, kernel_size=(7,), stride=(1,), padding=(3,))
            (2): Snake1d()
            (3): Conv1d(256, 256, kernel_size=(1,), stride=(1,))
          )
        )
        (1): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(256, 256, kernel_size=(7,), stride=(1,), padding=(9,), dilation=(3,))
            (2): Snake1d()
            (3): Conv1d(256, 256, kernel_size=(1,), stride=(1,))
          )
        )
        (2): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(256, 256, kernel_size=(7,), stride=(1,), padding=(27,), dilation=(9,))
            (2): Snake1d()
            (3): Conv1d(256, 256, kernel_size=(1,), stride=(1,))
          )
        )
        (3): Snake1d()
        (4): Conv1d(256, 512, kernel_size=(16,), stride=(8,), padding=(4,))
      )
    )
    (4): EncoderBlock(
      (block): Sequential(
        (0): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(512, 512, kernel_size=(7,), stride=(1,), padding=(3,))
            (2): Snake1d()
            (3): Conv1d(512, 512, kernel_size=(1,), stride=(1,))
          )
        )
        (1): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(512, 512, kernel_size=(7,), stride=(1,), padding=(9,), dilation=(3,))
            (2): Snake1d()
            (3): Conv1d(512, 512, kernel_size=(1,), stride=(1,))
          )
        )
        (2): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(512, 512, kernel_size=(7,), stride=(1,), padding=(27,), dilation=(9,))
            (2): Snake1d()
            (3): Conv1d(512, 512, kernel_size=(1,), stride=(1,))
          )
        )
        (3): Snake1d()
        (4): Conv1d(512, 1024, kernel_size=(16,), stride=(8,), padding=(4,))
      )
    )
    (5): Snake1d()
    (6): Conv1d(1024, 1024, kernel_size=(3,), stride=(1,), padding=(1,))
  )
)), ('quantizer', ResidualVectorQuantize(
  (quantizers): ModuleList(
    (0-8): 9 x VectorQuantize(
      (in_proj): Conv1d(1024, 8, kernel_size=(1,), stride=(1,))
      (out_proj): Conv1d(8, 1024, kernel_size=(1,), stride=(1,))
      (codebook): Embedding(1024, 8)
    )
  )
)), ('decoder', Decoder(
  (model): Sequential(
    (0): Conv1d(1024, 1536, kernel_size=(7,), stride=(1,), padding=(3,))
    (1): DecoderBlock(
      (block): Sequential(
        (0): Snake1d()
        (1): [ConvTranspose1d](https://pytorch.org/docs/stable/generated/torch.nn.ConvTranspose1d.html)((1536, 768, kernel_size=(16,), stride=(8,), padding=(4,))
        (2): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(768, 768, kernel_size=(7,), stride=(1,), padding=(3,))
            (2): Snake1d()
            (3): Conv1d(768, 768, kernel_size=(1,), stride=(1,))
          )
        )
        (3): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(768, 768, kernel_size=(7,), stride=(1,), padding=(9,), dilation=(3,))
            (2): Snake1d()
            (3): Conv1d(768, 768, kernel_size=(1,), stride=(1,))
          )
        )
        (4): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(768, 768, kernel_size=(7,), stride=(1,), padding=(27,), dilation=(9,))
            (2): Snake1d()
            (3): Conv1d(768, 768, kernel_size=(1,), stride=(1,))
          )
        )
      )
    )
    (2): DecoderBlock(
      (block): Sequential(
        (0): Snake1d()
        (1): ConvTranspose1d(768, 384, kernel_size=(16,), stride=(8,), padding=(4,))
        (2): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(384, 384, kernel_size=(7,), stride=(1,), padding=(3,))
            (2): Snake1d()
            (3): Conv1d(384, 384, kernel_size=(1,), stride=(1,))
          )
        )
        (3): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(384, 384, kernel_size=(7,), stride=(1,), padding=(9,), dilation=(3,))
            (2): Snake1d()
            (3): Conv1d(384, 384, kernel_size=(1,), stride=(1,))
          )
        )
        (4): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(384, 384, kernel_size=(7,), stride=(1,), padding=(27,), dilation=(9,))
            (2): Snake1d()
            (3): Conv1d(384, 384, kernel_size=(1,), stride=(1,))
          )
        )
      )
    )
    (3): DecoderBlock(
      (block): Sequential(
        (0): Snake1d()
        (1): ConvTranspose1d(384, 192, kernel_size=(8,), stride=(4,), padding=(2,))
        (2): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(192, 192, kernel_size=(7,), stride=(1,), padding=(3,))
            (2): Snake1d()
            (3): Conv1d(192, 192, kernel_size=(1,), stride=(1,))
          )
        )
        (3): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(192, 192, kernel_size=(7,), stride=(1,), padding=(9,), dilation=(3,))
            (2): Snake1d()
            (3): Conv1d(192, 192, kernel_size=(1,), stride=(1,))
          )
        )
        (4): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(192, 192, kernel_size=(7,), stride=(1,), padding=(27,), dilation=(9,))
            (2): Snake1d()
            (3): Conv1d(192, 192, kernel_size=(1,), stride=(1,))
          )
        )
      )
    )
    (4): DecoderBlock(
      (block): Sequential(
        (0): Snake1d()
        (1): ConvTranspose1d(192, 96, kernel_size=(4,), stride=(2,), padding=(1,))
        (2): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(96, 96, kernel_size=(7,), stride=(1,), padding=(3,))
            (2): Snake1d()
            (3): Conv1d(96, 96, kernel_size=(1,), stride=(1,))
          )
        )
        (3): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(96, 96, kernel_size=(7,), stride=(1,), padding=(9,), dilation=(3,))
            (2): Snake1d()
            (3): Conv1d(96, 96, kernel_size=(1,), stride=(1,))
          )
        )
        (4): ResidualUnit(
          (block): Sequential(
            (0): Snake1d()
            (1): Conv1d(96, 96, kernel_size=(7,), stride=(1,), padding=(27,), dilation=(9,))
            (2): Snake1d()
            (3): Conv1d(96, 96, kernel_size=(1,), stride=(1,))
          )
        )
      )
    )
    (5): Snake1d()
    (6): Conv1d(96, 1, kernel_size=(7,), stride=(1,), padding=(3,))
    (7): Tanh()
  )
))])
</pre></big>
