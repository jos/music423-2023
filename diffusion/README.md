# Main Recent Papers on Synthesis by Reverse Diffusion

- [2022-Jul - "Diffsound: Discrete Diffusion Model for Text-to-sound Generation"](Diffsound-2207.09983.pdf)
  [[HomePage](http://dongchaoyang.top/text-to-sound-synthesis-demo/)]
  [[Code](https://github.com/yangdongchao/text-to-sound-synthesis-demo)]
- [2023-Jan - "AudioLDM: Text-to-Audio Generation with Latent Diffusion Models"](AudioLDM-2301.12503.pdf)
  [[HomePage](https://audioldm.github.io)]
  [[Code](https://github.com/haoheliu/AudioLDM)]
- [2023-Jan - "Moûsai: Efficient Text-to-Music Diffusion Models"](Mousai-2301.11757.pdf)
  [[HomePage](https://diligent-pansy-4cb.notion.site/Music-Generation-with-Diffusion-ebe6e9e528984fa1b226d408f6002d87)]
  [[Code](https://github.com/archinetai/audio-diffusion-pytorch)]
- [2023-Feb - "Noise2Music: Text-conditioned Music Generation with Diffusion Models"](Noise2Music-2302.03917.pdf)
  [[HomePage](https://google-research.github.io/noise2music/)]
- [2023-Apr - "Text-to-Audio Generation using Instruction-Guided Latent Diffusion Model"](Tango.pdf)
  [[HomePage](https://tango-web.github.io/)]
  [[Code](https://github.com/declare-lab/tango)]
- [2023-Aug - MBD: "From Discrete Tokens to High-Fidelity Audio Using Multi-Band Diffusion"](MultiBandDiffusion-2308.02560.pdf)
  [[Doc](https://github.com/facebookresearch/audiocraft/blob/main/docs/MBD.md)]
  [[Code](https://github.com/facebookresearch/audiocraft/tree/main)]<br/>
  ([EnCodec](../ai-audio-codecs/EnCodec-2210.13438.pdf) compatible alternative in [MusicGen](../ai-music-audio-gen/MusicGen-2306.05284.pdf))
- [2023-Oct - "Generation or Replication: Auscultating Audio Latent Diffusion Models"](Generation-2310.10604.pdf)
- [2024-Jan - DITTO: Diffusion Inference-Time T-Optimization for Music Generation](DITTO-2401.12179v1.pdf)
- [2024-Apr - "Long-Form Music Generation with Latent Diffusion"](../ai-music-audio-gen/DiffusionLongFormMusic-2404.10301.pdf)
  
[[Demos](https://ai.honu.io/papers/mbd/)]

## Initial Diffusion Papers and selected later papers
- [2015: "Deep Unsupervised Learning using Nonequilibrium Thermodynamics"](Diffusion-1503.03585.pdf)
- [2020: "Denoising Diffusion Probabilistic Models"](Diffusion-Ho-2006.11239.pdf)
- [2021: "Score-Based Diffusion"](ScoreBasedDiffusion-YangSong-2011.13456v2.pdf)
- [2022: DDIM: "Denoising Diffusion Implicit Models"](DDIM-DenoisingDiffusionImplicitModels-2010.02502v4.pdf)
- [2024: EMD: "EM Distillation for One-step Diffusion Models"](EMD-OneStepDiffusion-2405.16852v1.pdf)

## Diffusion Tutorials
- [Understanding Diffusion Models: A Unified Perspective](https://calvinyluo.com/2022/08/26/diffusion-tutorial.html)
- [The Annotated Diffusion Model](https://huggingface.co/blog/annotated-diffusion)
- [Tutorial on Diffusion Models for Imaging and Vision](DiffusionTutorial-2403.18103.pdf)
- [Video: Diffusion and Score-Based Generative Models](https://www.youtube.com/watch?v=wMmqCMwuM2Q)

[Code: Riffusion](https://github.com/riffusion/riffusion.git)



---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
