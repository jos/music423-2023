# Attention papers

- [Additive Attention](AdditiveAttention-1409.0473.pdf)
- [Attention is All you Need](Attention-Xformers-1706.03762.pdf)
- [Relative-Position Attention](SelfAttentionRelativePosition-1803.02155.pdf)
- [Multi-Query Attention (MQA)](MQA-1911.02150.pdf)
- [Longformer](Longformer-2004.05150.pdf)
- [Transformers as RNNs](TransformersAsRNNs-2006.16236.pdf)
- [RoPE](RoPE-2104.09864.pdf)
- [AST: Audio Spectrogram Transformer](AST-AudioSpectrogramTransformer-2104.01778v3.pdf)
- [Attention-Free Transformer](AttentionFreeXformer-2105.14103.pdf)
- [ALiBi](ALiBi-2108.12409.pdf)
- [Flash Attention](FlashAttention-2205.14135.pdf)
<!-- - [Ring Attention 1](RingAttention-2310.01889.pdf) -->
- [Grouped-Query Attention (MQA)](GQA-GroupedQueryAttention-2305.13245.pdf)
- [Gated Linear Attention](GLA-GatedLinearAttention-2312.06635.pdf)
- [Ring Attention](RingAttention2-2402.08268.pdf)
- [DenseFormer](DenseFormer-2402.02622.pdf)
- [Based](Based-2402.18668.pdf)
- [Jamba](Jamba-2403.19887.pdf)

## Misc
- ["Standard Transformer" 2024 (Developments since 2017)](TransformerEvolution.jpeg)<br/>
  From [Tweet](https://twitter.com/Muhtasham9/status/1772469982485438485/photo/1)

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
