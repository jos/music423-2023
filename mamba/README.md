# Suggested Paper Sequence Leading Up to Mamba 2023 and Beyond

- [HiPPO](HiPPO-2008.07669.pdf)
- [S4](S4-2111.00396.pdf)
- [SaShiMi](SaShiMi-2202.09729.pdf)
- [DSS](DSS-2203.14343.pdf)
- [S4D](S4D-2206.11893.pdf)
- [S5](S5-2208.04933.pdf)
- [H3](H3-2212.14052.pdf)
- [LRU](LRU-2303.06349.pdf)
- [RWKV](RWKV-2305.13048.pdf)
- [Mamba](Mamba-2312.00752.pdf)
- [Hawk (and Griffin)](Griffin-2402.19427.pdf)
- [Jamba (adds attention)](Jamba-2403.19887.pdf)
- [Mamba360 (survey)](Mamba360-2404.16112v1.pdf)

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
