# Mamba Paper Sequence Annotated

* [HiPPO](HiPPO-2008.07669.pdf)
* [S4](S4-2111.00396.pdf)
* [SaShiMi](SaShiMi-2202.09729.pdf)
* [DSS](DSS-2203.14343.pdf)
* [S4D](S4D-2206.11893.pdf)
* [S5](S5-2208.04933.pdf)
* [H3](H3-2212.14052.pdf)
* [LRU](LRU-2303.06349.pdf)
  - Making RNNs as good as SSMs:<br/>
    "we analyze and ablate a series of changes to standard RNNs
       including _linearizing_ and _diagonalizing_ the recurrence, using
          better _parameterizations_ and _initializations_, and ensuring
             proper _normalization_ of the forward pass."
* [RWKV](RWKV-2305.13048.pdf)
* [Mamba](Mamba-2312.00752.pdf)
* [Griffin](Griffin-2402.19427.pdf)
  - Griffin combines the gated linear recurrence of Mamba with local
    attention from the Transformer world and seems to be the current
    leader (as of 2024-03-03).

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
