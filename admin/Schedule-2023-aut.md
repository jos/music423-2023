## Seminar Schedule, Autumn 2023

1. 09/29: Introductions and Planning - What are we all interested in these days?
2. 10/06:
   - JOS: [Finite Scalar Quantization (FSQ)](./ai-audio-codecs/FSQ-FiniteScalarQuantization-2309.15505.pdf) - quick overview
   - Josh Mitchell (MA/MST): Nonlinear Circuit Modeling - update and outlook
3. 10/13:
   - Updates all around
   - JOS: AI Audio Codecs for Music Audio Generation
4. 10/20:
   - Soohyun update
   - Senyuan Fan, Emily Kuo, Sneha Shah, and Marina Bosi: "Transient Detection Methods for Audio Coding"
5. 10/27:
   - JOS ADC-2023 Pre-presentation on Spectral Modeling and AI Audio Codecs
   - Andrew Romero and Ashwin Alinkil on Diffusion based Drum Synthesis
6. 11/03: No meeting
7. 11/10: Final-Year UCSD PhD/CS&E Student Hao-Wen Dong summarizing his recent research on MuseGAN, Arranger, Deep Performer, CLIPSep, CLIPSonic, and Multitrack Music Performer (presented at AAAI, ISMIR, ICASSP, ICLR, and WASPAA)
8. 11/17: Travis Skare (CCRMA PhD/EE student): GPU percussion synthesis, Part 1
9. 12/01: Travis Skare (CCRMA PhD/EE student): GPU percussion synthesis, Part 2
10. 12/08: Soohyun Kim: [Physics Informed Neural Networks (PINN)](./pinn/PINN - Soohyun Kim.pdf)

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
