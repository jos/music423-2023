# WaveNet papers

- [2016/06 - "Conditional Image Generation with PixelCNN Decoders"](PixelCNN-1606.05328v2.pdf)
- [2016/09 - WaveNet: "A Generative Model for Raw Audio"](WaveNet-2016-1609.03499.pdf)
- [2016/11 - "Fast WaveNet Generation Algorithm"](FastWaveNet-1611.09482v1.pdf)
- [2017/02 - DeepVoice: "Deep Voice: Real-time Neural Text-to-Speech"](DeepVoiceWaveNet-1702.07825v2.pdf)
- [2017/06 - FeedForwardWaveNet: Speech Denoising"](FeedForwardWaveNetSpeechDenoising-1706.07162v3.pdf)
- [2017/11 - "Parallel WaveNet: Fast High-Fidelity Speech Synthesis"](ParallelWaveNet-1711.10433v1.pdf)
- [2017/12 - Tacotron 2: "Natural TTS Synthesis by Conditioning WaveNet on Mel Spectrogram Predictions"](Tacotron2-1712.05884v2.pdf)
- [2019/01 - "Real-Time Modeling of Audio Distortion Circuits with Deep Learning"](ELEC_Damskagg_Real_time_modeling_SMC2019.pdf)
- [2019/09 - "Real-time Guitar Amplifier Emulation with Deep Learning"](AlecWrightAmpModel-DAFx2019.pdf)
- [2020/10 - "Real-Time Neural Network Virtual Analog Audio Effects"](AlecWrightRealTimeAmpModeling20.pdf)
- [2023/12 - "Neural Modeling of Audio Effects"](AlecWrightT-NeuralModelingAudioFX-isbn9789526415765.pdf)

---

[Notes](Notes.md)

---

#### [Music 423 2023 GitLab Project](https://cm-gitlab.stanford.edu/jos/music423-2023/-/blob/master/README.md)
